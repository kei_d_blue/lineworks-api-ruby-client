# Lineworks::Api::Ruby::Client

## Installation


```ruby
gem 'lineworks-api-ruby-client'
```

# 秘密鍵のBase64化
LINEWORKで取得したサーバーキーをBase64エンコードして環境変数としてセットする方法

```
cat private_20200907165739.key | base64 | sed -e :loop -e 'N; $!b loop' -e 's/\n//g' 
```




# バージョン2
## 環境変数
以下の環境変数を記載しておくことで、AuthServerServiesの初期化を省略できます。
```
ENV["LINEWORKS_CLIENT_ID"]
ENV["LINEWORKS_CLIENT_SECRET"]
ENV['LINEWORKS_SERVICE_ACCOUNT']
ENV["LINEWORKS_SCOPE"]
ENV["LINEWORKS_RSA_PRIVATE_KEY"]
```

## 使い方
```
require "lineworks/apis/v2.0/auth/service_account"
require "lineworks/apis/v2.0/user"

talk = Lineworks::Apis::Bot::Message.new
talk.authorization = auth.get_server_token
p talk.messageForUserId(bot_id: <bot_id>, user_id: <uuid>, message: {
   content: {
       type: "text",
       text: "message test."
   }
})
```

```
require "lineworks/apis/v2.0/auth/service_account"
require "lineworks/apis/v2.0/user"

user = Lineworks::Apis::User.new
user.authorization = talk.authorization
response = user.list().body
response = user.list(cursor: response["responseMetaData"]["nextCursor"]).body

```

# バージョン1
## 環境変数
以下の環境変数を記載しておくことで、AuthServerServiesの初期化を省略できます。
```
ENV["LINEWORKS_APP_ID"]
ENV["LINEWORKS_CONSUMER_KEY"]
ENV['LINEWORKS_DOMAIN']
ENV["LINEWORKS_SERVER_ID"]
```

## 使い方（サーバーAPI）
```
require "lineworks/apis/auth/auth_service"
require "lineworks/apis/message/talk_bot_service"

auth = Lineworks::Apis::Auth::AuthServerService.new(
    app_id:"<app_id?", 
    consumer_key:"<consumer_key>", 
    domain:"<domain>", 
    server_id:"<server_id>", 
    private_key:"<private_key_path>"
)

auth.set_rsa_private_key_by_base64("<base64文字列>")

talk = Lineworks::Apis::Message::TalkBotService.new
talk.authorization = auth.get_server_token
talk.send_message(bot_no:<bot_no>, accountId:"<to>", message:"<message>")
```


