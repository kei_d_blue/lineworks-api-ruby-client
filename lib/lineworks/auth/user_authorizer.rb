require "multi_json"

require "lineworks/apis/auth/auth_service"

module Lineworks
    module Auth
        class UserAuthorizer
            MISMATCHED_CLIENT_ID_ERROR = "Token APP_ID of %s does not match configured APP_ID %s".freeze

            # 初期化
            def initialize(app_id:nil, consumer_key:nil, domain:nil, token_store:)
                @app_id = app_id
                @consumer_key = consumer_key
                @domain = domain
                @token_store = token_store
            end

            # 認証情報の取得
            def get_credentials(user_id:)
                saved_token = stored_token(user_id: user_id)
                return nil if saved_token.nil?

                data = MultiJson.load(saved_token)
        
                if data.fetch("app_id", @app_id) != @app_id
                    raise format(MISMATCHED_CLIENT_ID_ERROR, data["app_id"], @app_id)
                end
                
                # 付随データを追加
                data["consumer_key"] = @consumer_key
                data["domain"]       = @domain

                return data
            end

            # auth_urlを表示
            def get_authorization_url(redirect_url:)
                auth_service = Lineworks::Apis::Auth::AuthService.new(app_id:@app_id, consumer_key:@consumer_key, domain:@domain)
                auth_service.get_authorization_url(redirect_url: redirect_url)
            end

            # authoziation_codeから認証データを生成
            def get_credentials_from_code(user_id:,code:,redirect_url:)
                auth_service = Lineworks::Apis::Auth::AuthService.new(app_id:@app_id, consumer_key:@consumer_key, domain:@domain)
                credentials = auth_service.get_credentials_from_code(code: code)

                # 付随データを追加
                credentials["app_id"]       = @app_id
                credentials["consumer_key"] = @consumer_key
                credentials["domain"]       = @domain
                credentials
            end

            # authorization_codeから認証データｗ生成し、保存
            def get_and_store_credentials_from_code(user_id:, code:, redirect_url:)
                credentials = get_credentials_from_code(user_id:user_id,code:code,redirect_url:redirect_url)
                store_credentials(user_id:user_id, credentials:credentials)
            end

            # 認証データを保存
            def store_credentials(user_id:,credentials:)
                json = MultiJson.dump(
                    app_id: @app_id,
                    access_token: credentials["access_token"],
                    refresh_token: credentials["refresh_token"],
                    expire_in: credentials["expire_in"]
                )
                @token_store.store user_id, json
                credentials
            end

            private
            # storeされているトークン取得
            def stored_token(user_id:)
                @token_store.load user_id
            end            
        end
    end
end