require "yaml/store"

# Lineworksネームスペース
module Lineworks

    module Auth
        module Stores
            class FileTokenStore
                def initialize(file:)
                    @store = YAML::Store.new file
                end

                # (see Google::Auth::Stores::TokenStore#load)
                def load id
                    @store.transaction { @store[id] }
                end

                # (see Google::Auth::Stores::TokenStore#store)
                def store id, token
                    @store.transaction { @store[id] = token }
                end

                # (see Google::Auth::Stores::TokenStore#delete)
                def delete id
                    @store.transaction { @store.delete id }
                end
            end
        end
    end
end