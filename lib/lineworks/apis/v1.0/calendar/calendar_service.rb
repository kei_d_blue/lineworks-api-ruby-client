require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/basic_service'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # カレンダーAPI
        # @author Keita Suzuki
        # @abstract atest
        # @see https://developers.worksmobile.com/kr/document/1007001?lang=ja LINEWORKS API Document
        module Calendar
            module Response
                module Parser
                    # 戻り値オブジェクトに特異メソッド itemsを定義
                    module Schedule
                        def items
                            rows = []
                            self.return_value.each do |value|
                                schedule = Lineworks::Apis::Calendar::Schedule.new(
                                                user_id:     value["userId"],
                                                calendar_id: value["calendarId"],
                                                ical:        ::Icalendar::Event.parse(value["ical"]).first,
                                                view_url:    value["viewUrl"] )
                                rows.push(schedule)
                            end
                            rows
                        end
                    end
                end
            end

            # スケジュールオブジェクト
            # ical上では Event だが、LINEWORKSではScheduleと呼んでいるので
            class Schedule
                attr_reader :user_id
                attr_reader :calendar_id
                attr_accessor :ical
                attr_reader :view_url

                # 自身に存在しないメソッドでも@icalに定義されていれば返却する
                # 引数が定義されている場合はsendに引数も渡す
                def method_missing(method, *params)
                    if @ical.respond_to?(method)
                        if @ical.method(method).arity > 0
                            @ical.send(method, params)
                        else
                            @ical.send(method)
                        end
                    else
                        super
                    end
                end

                # 初期化
                def initialize(user_id:nil, calendar_id:nil, ical:nil, view_url:nil)
                    @user_id = user_id
                    @calendar_id = calendar_id
                    @ical = ical || ::Icalendar::Event.new
                    @view_url = view_url
                end

                # ical形式の文字列を生成
                def to_ical
                    # ical データの作成
                    cal = ::Icalendar::Calendar.new
                    cal.add_event @ical
                    cal.to_ical
                end
            end

            # カレンダーAPIクライアント
            # @author Keita Suzuki
            # @abstract atest
            # @see https://developers.worksmobile.com/kr/document/1007001?lang=ja LINEWORKS API Document
            class CalendarService < Apis::Core::BasicService
                
                # 基本カレンダーID照会
                def get_default_calendar_id
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/calendar/getDefaultCalendarId")
                        set_default_headers(req)                    
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        res.extend Lineworks::Apis::Core::Response::Parser::ReturnValue
                    end
                end

                # カレンダー情報の取得
                # @see https://developers.worksmobile.com/kr/document/1007003?lang=ja
                def get_calendar(calendar_id:)
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/calendar/getCalendar")
                        set_default_headers(req)

                        req.body = JSON.dump({
                            calendarId: calendar_id
                        })                    
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        res.extend Lineworks::Apis::Core::Response::Parser::ReturnValue
                    end                    
                end
            
                # カレンダーリスト照会
                # @see https://developers.worksmobile.com/kr/document/1007006?lang=ja
                def users_me_calendar_list
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.get do |req|
                        set_url_r(req, "/calendar/rest/v1/users/me/calendarList")
                        set_default_headers(req)
                    end

                    # 結果の取得
                    if res.body == nil
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    end

                    res
                end

                # スケジュールリストの閲覧
                # @see https://developers.worksmobile.com/kr/document/1007015?lang=ja LINEWORKS
                def get_schedule_list(calendar_id:nil, range_date_from:, range_date_until:)
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/calendar/getScheduleList/V3")
                        set_default_headers(req)
                    
                        req.body = JSON.dump({
                            calendarId: calendar_id,
                            rangeDateFrom: range_date_from,
                            rangeDateUntil: range_date_until
                        })
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        res.extend Lineworks::Apis::Core::Response::Parser::ReturnValue
                        res.extend Lineworks::Apis::Calendar::Response::Parser::Schedule                    
                    end
                end
            
            
                def create_schedule(calendar_id:, schedule_object:)
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/calendar/createSchedule")
                        set_default_headers(req)
                    
                        req.body = JSON.dump({
                            calendarId: calendar_id,
                            scheduleIcalString: schedule_object.to_ical,
                        })
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        res                
                    end                
                end

                private

                # app_idを含んだurlを設定する
                # 頭に/r/を含んだ特殊なURL
                def set_url_r(req, url)
                    req.url "/r/#{@authorization["app_id"]}#{url}"
                end
            end
        end
    end
end
