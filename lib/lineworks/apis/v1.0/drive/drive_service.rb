require 'faraday'
require "active_support/all"

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/basic_service'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # カレンダーAPI
        # @author Keita Suzuki
        # @abstract atest
        # @see https://developers.worksmobile.com/kr/document/1007001?lang=ja LINEWORKS API Document
        module Drive

            # DriverAPIクライアント
            # @author Keita Suzuki
            # @abstract atest
            # @see https://developers.worksmobile.com/kr/document/1007001?lang=ja LINEWORKS API Document
            class DriveService < Apis::Core::BasicService
                attr_accessor :authorization

                # 基本カレンダーID照会
                def get_user_info
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/drive/getUserInfo/v1")
                        set_default_headers(req)
                    end

                    # 結果の取得
                    p res.body
                end

                def get_domain_contact
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.get do |req|
                        set_url(req, "/contact/getDomainContact/v1")
                        set_default_headers(req)

                    end       
                    p res.body                    
                end

                
                def chart
                    # コネクションの生成
                    conn = get_connection(url:"https://contact.worksmobile.com")
                    conn.builder.delete(:json)

                    # リクエスト
                    res = conn.get do |req|
                        req.url "/v2/domain/contacts/profileWithAdditional"
                        #req.headers['Content-Type'] = 'application/json;charset=UTF-8'
                        req.headers['consumerKey'] = ENV["LINEWORKS_CONSUMER_KEY"]
                        req.headers['Authorization'] = "Bearer #{self.authorization['access_token']}"

                    end       
                                         # 結果の取得
                    puts res.body
                end
            end
        end
    end
end