require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/basic_service'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # トーク Bot API
        # @author Keita Suzuki
        # @abstract atest
        module Message

            
            # トーク Bot APIクライアント
            # @author Keita Suzuki
            # @abstract atest
            # @see https://developers.worksmobile.com/jp/document/3005001?lang=ja LINEWORKS API Document
            class TalkBotService < Apis::Core::BasicService
                
                # Lineworksにメッセージを送信
                # 廃止する
                # @param token サーバートークン
                # @param accountId 返信先ID
                # @param message メッセージ
                # @see https://developers.worksmobile.com/jp/document/1005008?lang=ja 
                def send_message(bot_no:, accountId:, message:)
                    push_message(bot_no: bot_no, message: {
                        "accountId": accountId,
                        "content": {
                            "type": "text",
                            "text": message
                        }
                    })
                end

                # Lineworksにメッセージを送信
                # @param token サーバートークン
                # @param accountId 返信先ID
                # @param message メッセージ
                # @see https://developers.worksmobile.com/jp/document/1005008?lang=ja 
                def push_message(bot_no:, message:)
                    # コネクションの生成
                    conn = get_connection

                    res = conn.post do |req|
                        req.url ("/r/#{@authorization["app_id"]}/message/v1/bot/#{bot_no}/message/push")
                        set_default_headers(req)  
                
                        req.body = JSON.dump(message)
                    end
                end

                # Bot を含むトークルーム作成
                # @param token サーバートークン
                # @param accountId 返信先ID
                # @param message メッセージ
                # @see https://developers.worksmobile.com/jp/document/1005022?lang=ja
                def create_room(bot_no:, accountIds:, title:nil)
                    # コネクションの生成
                    conn = get_connection

                    res = conn.post do |req|
                        req.url ("/r/#{@authorization["app_id"]}/message/v1/bot/#{bot_no}/room")
                        set_default_headers(req)

                        postdata = {
                            "accountIds": accountIds
                        }
                        postdata["title"] = title if title
                
                        req.body = JSON.dump(postdata)
                    end
                end
                
                # メンバーIDの紹介
                def get_member_list(bot_no, domain_id)
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/message/getMemberList")
                        set_default_headers(req)     
                        req.body = JSON.dump({
                            botNo: bot_no,
                            domainId: domain_id
                        })               
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        res.extend Lineworks::Apis::Core::Response::Parser::ReturnValue
                    end

                    res
                end
            end
        end
    end
end
