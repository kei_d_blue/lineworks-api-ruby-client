require "active_support/all"

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/basic_service'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # アドレス帳
        # @author Keita Suzuki
        # @abstract atest
        # @see https://developers.worksmobile.com/kr/document/1006001?lang=ja
        module Contact

            # DriverAPIクライアント
            # @author Keita Suzuki
            # @abstract atest
            # @see https://developers.worksmobile.com/kr/document/1007001?lang=ja LINEWORKS API Document
            class ContactService < Apis::Core::BasicService
                attr_accessor :authorization

                def create_schedule(calendar_id:, schedule_object:)
                    # コネクションの生成
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        set_url(req, "/contact/listSharedGroup/v2")
                        set_default_headers(req)
                    
                    end

                    # 結果の取得
                    if res.body["result"] != "success"
                        error_message = JSON.parse(res.body["errorMessage"])
                        raise Lineworks::Apis::Core::InvalidResponseException.new(message:error_message["result"], body: res.body)
                    else
                        p res                
                    end                
                end
            end
        end
    end
end
