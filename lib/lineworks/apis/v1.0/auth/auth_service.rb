require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/basic_service'

require "base64"

# 正規表現でStringを拡張
class String
    def encodeURIComponent
        unescaped_form = /([#{Regexp.escape(';/?:@&=+$,<>#%"{}|\\^[]`' + (0x0..0x1f).map{|c| c.chr }.join + "\x7f" + (0x80..0xff).map{|c| c.chr }.join)}])/n
        self.force_encoding("ASCII-8BIT").gsub(unescaped_form){ "%%%02X" % $1.ord } 
    end

    def encodeURI
        unescaped = /([#{Regexp.escape('<>#%"{}|\\^[]`' + (0x0..0x1f).map{|c| c.chr }.join + "\x7f" + (0x80..0xff).map{|c| c.chr }.join)}])/n
        self.force_encoding("ASCII-8BIT").gsub(unescaped){ "%%%02X" % $1.ord } 
    end
end

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # 認証ネームスペース
        module Auth

            # API 認証の準備
            # https://developers.worksmobile.com/kr/document/1002002?lang=ja
            class AuthService < Apis::Core::BasicService

                # 初期化
                def initialize(app_id:nil, consumer_key:nil, domain:nil)
                    super
                    # データ定義
                    @app_id = app_id || ENV["LINEWORKS_APP_ID"]
                    @consumer_key = consumer_key || ENV["LINEWORKS_CONSUMER_KEY"]
                    @domain = domain || ENV['LINEWORKS_DOMAIN']

                    @base_url = "https://auth.worksmobile.com"
                    @auth_uri = "/ba/#{@app_id}/service/authorize"
                    @token_uri = "/ba/#{@app_id}/service/token"                    
                end

                # サービス API Authorization Code の発行
                # https://developers.worksmobile.com/kr/document/1002002?lang=ja#-api-authorization-code-
                def get_authorization_url(redirect_url:'http://localhost')
                    "#{@base_url}#{@auth_uri}?client_id=#{@consumer_key}&redirect_uri=#{redirect_url}&domain=#{@domain}"
                end

                # サービス API Access Token の発行
                # https://developers.worksmobile.com/kr/document/1002002?lang=ja#-api-access-token-
                def get_credentials_from_code(code:)
                    conn = get_connection

                    # リクエスト
                    res = conn.post do |req|
                        req.url @token_uri

                        req.body = {
                            client_id: @consumer_key,
                            code: code.strip,
                            domain: @domain
                        }            
                    end

                    res.body
                end
            end

            # API 認証
            # 
            class AuthServerService < Apis::Core::BasicService

                # 初期化
                def initialize(app_id:nil, consumer_key:nil, domain:nil, server_id:nil, private_key:nil)
                    # データ定義
                    @app_id          =       app_id || ENV["LINEWORKS_APP_ID"]
                    @consumer_key    = consumer_key || ENV["LINEWORKS_CONSUMER_KEY"]
                    @domain          =       domain || ENV['LINEWORKS_DOMAIN']
                    @server_id       = server_id    || ENV["LINEWORKS_SERVER_ID"]
                    @rsa_private_key = private_key  
                    
                    if !@rsa_private_key && ENV['LINEWORKS_RSA_PRIVATE_KEY']
                        set_rsa_private_key_by_base64(ENV['LINEWORKS_RSA_PRIVATE_KEY'])
                    elsif @rsa_private_key
                        set_rsa_private_key_by_file(@rsa_private_key )
                    end


                    @base_url = "https://authapi.worksmobile.com"
                    @token_uri = "/b/#{@app_id}/server/token"                   
                end

                def set_rsa_private_key_by_file(path)
                    @rsa_private_key = File.new(path)
                end

                def set_rsa_private_key_by_base64(str)
                    @rsa_private_key = Base64.decode64(str)
                end

                # サーバートークンを取得します。
                def get_server_token
                    # jwtを取得
                    token = generate_jwt_token

                    conn = get_connection
                    res = conn.post do |req|
                        req.url  @token_uri
                        req.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
                        req.body = {
                            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer".encodeURIComponent,
                            "assertion": token
                        }
                    end
                    res = res.body
                    res["app_id"] = @app_id
                    res["consumer_key"] = @consumer_key

                    res
                end
 
                # JWTトークンの生成
                def generate_jwt_token
                    private_key = OpenSSL::PKey::RSA.new @rsa_private_key 
                    
                    iss = @server_id 
                    iat = Time.now.to_i
                    exp = iat + (60 * 60)

                    payload = {
                        "iss": iss,
                        "iat": iat,
                        "exp": exp
                    }
                    token = JWT.encode(payload,private_key, 'RS256')
                end                
            end
        end
    end
end