require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/http_request'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # トーク Bot API
        # @author Keita Suzuki
        # @see https://developers.worksmobile.com/jp/reference/bot?lang=ja
        class Bot < Apis::Core::HttpRequest

            # Bot の取得
            # @param bot_id: ボットID
            # @see https://developers.worksmobile.com/jp/docs/bot-get
            def get(bot_id:)
                # コネクションの生成
                conn = get_connection

                res = conn.get do |req|
                    req.url ("/v1.0/bots/#{bot_id}")
                    set_default_headers(req)  
                end
            end

            # Bot リストの取得
            # @param bot_id: ボットID
            # @see https://developers.worksmobile.com/jp/docs/bot-list
            def list()
                # コネクションの生成
                conn = get_connection
                
                res = conn.get do |req|
                    req.url ("/v1.0/bots")
                    set_default_headers(req)  
                end
            end
        end
    end
end
