require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/http_request'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # トーク Bot API
        # @author Keita Suzuki
        # @see https://developers.worksmobile.com/jp/reference/bot?lang=ja
        class Bot < Apis::Core::HttpRequest

            # ドメイン上のBot管理
            # https://developers.worksmobile.com/jp/docs/bot-domain-register
            class Domains < Apis::Core::HttpRequest

                # Bot のドメインリストの取得
                # @param bot_id: ボットID
                # @see https://developers.worksmobile.com/jp/docs/bot-domain-list
                def list(bot_id:)
                    # コネクションの生成
                    conn = get_connection

                    res = conn.get do |req|
                        req.url ("/v1.0/bots/#{bot_id}/domains")
                        set_default_headers(req)  
                    end
                end

                # Bot のドメイン登録
                # @param bot_id: ボットID
                # @param domain_id: ドメインID
                # @see https://developers.worksmobile.com/jp/docs/bot-domain-register
                def register(bot_id:, domain_id:)
                    # コネクションの生成
                    conn = get_connection
                    
                    res = conn.post do |req|
                        req.url ("/v1.0/bots/#{bot_id}/domains/#{domain_id}")
                        set_default_headers(req)  
                    end
                end

                # Bot 利用ユーザーリストの取得
                # @param bot_id: ボットID
                # @param domain_id: ドメインID
                # @see https://developers.worksmobile.com/jp/docs/bot-domain-member-list
                def memberList(bot_id:, domain_id:)
                    # コネクションの生成
                    conn = get_connection
                    
                    res = conn.get do |req|
                        req.url ("/v1.0/bots/#{bot_id}/domains/#{domain_id}/members?count=100")
                        set_default_headers(req)  
                    end
                end

                # Bot 利用ユーザーの登録
                # @param bot_id: ボットID
                # @param domain_id: ドメインID
                # @param user_id: ユーザーID
                # @see https://developers.worksmobile.com/jp/docs/bot-domain-member-create
                def memberCreate(bot_id:, domain_id:, user_id:)
                    # コネクションの生成
                    conn = get_connection
                    
                    res = conn.post do |req|
                        req.url ("/v1.0/bots/#{bot_id}/domains/#{domain_id}/members")
                        set_default_headers(req)

                        req.body = JSON.dump({
                            userId: user_id
                        })
                    end
                end
            end
        end
    end
end
