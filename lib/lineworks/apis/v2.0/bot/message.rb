require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/http_request'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # トーク Bot API
        # @author Keita Suzuki
        # @see https://developers.worksmobile.com/jp/reference/bot?lang=ja
        class Bot < Apis::Core::HttpRequest

            
            # メッセージ送信
            # @see https://developers.worksmobile.com/jp/docs/bot-api LINEWORKS API Document
            class Message < Apis::Core::HttpRequest

                # Lineworksにメッセージを送信
                # @param bot_id: ボットID
                # @param user_id: 送信先
                # @param message: メッセージ
                # @see https://developers.worksmobile.com/jp/reference/bot-user-message-send?lang=ja
                def messageForUserId(bot_id:, user_id:, message:)
                    # コネクションの生成
                    conn = get_connection

                    res = conn.post do |req|
                        req.url ("/v1.0/bots/#{bot_id}/users/#{user_id}/messages")
                        set_default_headers(req)  
                
                        req.body = JSON.dump(message)
                    end
                end

                # Lineworksにメッセージを送信
                # @param bot_id: ボットID
                # @param channel_id: 送信先
                # @param message: メッセージ
                # @see https://developers.worksmobile.com/jp/reference/bot-channel-message-send?lang=ja
                def messageForChannelId(bot_id:, channel_id:, message:)
                    # コネクションの生成
                    conn = get_connection

                    res = conn.post do |req|
                        req.url ("/v1.0/bots/#{bot_id}/channels/#{channel_id}/messages")
                        set_default_headers(req)  
                
                        req.body = JSON.dump(message)
                    end
                end
            end
        end
    end
end
