require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/http_request'
require "base64"

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # 認証ネームスペース
        module Auth

            # Service Account認証 (JWT)
            # https://developers.worksmobile.com/jp/reference/authorization-sa?lang=ja
            class ServiceAccount < Apis::Core::HttpRequest

                # 初期化
                def initialize()
                    # データ定義
                    self.client_id(ENV["LINEWORKS_CLIENT_ID"])
                    self.client_secret(ENV["LINEWORKS_CLIENT_SECRET"])
                    self.service_account(ENV["LINEWORKS_SERVICE_ACCOUNT"])
                    self.scope(ENV["LINEWORKS_SCOPE"])
                    self.rsa_private_key_by_base64(ENV['LINEWORKS_RSA_PRIVATE_KEY'])

                    @base_url = "https://auth.worksmobile.com"
                    @token_uri = "/oauth2/v2.0/token"                   
                end

                # クライアントID
                def client_id(client_id)
                    @client_id = client_id
                    self
                end

                # クライアントシークレット
                def client_secret(client_secret)
                    @client_secret = client_secret
                    self
                end

                # サービスアカウント
                def service_account(service_account)
                    @service_account = service_account
                    self
                end

                # スコープ
                def scope(scope)
                    @scope = scope
                    self
                end

                # RSA秘密鍵をファイルからセット
                def rsa_private_key_by_file(path)
                    @rsa_private_key = File.new(path)
                    self
                end

                # RSA秘密鍵をBASE64エンコード文字列からセット
                def rsa_private_key_by_base64(str)
                    @rsa_private_key = Base64.decode64(str) if str
                    self
                end

                # サーバートークンを取得します。
                def get_server_token
                    # jwtを取得
                    token = generate_jwt_token

                    conn = get_connection
                    res = conn.post do |req|
                        req.url  @token_uri
                        req.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
                        req.body = {
                            "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
                            "assertion": token,
                            "client_id": @client_id,
                            "client_secret": @client_secret,
                            "scope": @scope 
                        }
                    end
                    res = res.body
                    res["client_id"] = @client_id
                    

                    res
                end
 
                # JWTトークンの生成
                def generate_jwt_token
                    private_key = OpenSSL::PKey::RSA.new @rsa_private_key 

                    iat = Time.now.to_i
                    exp = iat + (60 * 60)

                    payload = {
                        "iss": @client_id ,
                        "sub": @service_account,
                        "iat": iat,
                        "exp": exp
                    }
                    token = JWT.encode(payload,private_key, 'RS256')
                end                
            end
        end
    end
end