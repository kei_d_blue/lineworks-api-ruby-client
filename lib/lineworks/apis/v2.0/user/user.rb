require 'faraday'
require "active_support/all"
require 'icalendar'
require 'icalendar/tzinfo'

require 'uri'

require 'lineworks/apis/core/error'
require 'lineworks/apis/core/response'
require 'lineworks/apis/core/http_request'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # UserAPI
        # @author Keita Suzuki
        # @see https://developers.worksmobile.com/jp/reference/user-create?lang=ja
        class User < Apis::Core::HttpRequest

            # ユーザーリストの取得
            # @see https://developers.worksmobile.com/jp/reference/user-list?lang=ja
            def list(cursor:nil)
                # コネクションの生成
                conn = get_connection

                res = conn.get do |req|
                    req.url ("/v1.0/users")
                    req.params["cursor"] = cursor unless cursor.nil?
                    set_default_headers(req)  
                end
            end

            # ユーザーの取得
            # @param user_id: 送信先
            # @see https://developers.worksmobile.com/jp/reference/user-get?lang=ja
            def get(user_id:)
                # コネクションの生成
                conn = get_connection

                res = conn.get do |req|
                    req.url ("/v1.0/users/#{user_id}")
                    set_default_headers(req)  
                end
            end
        end
    end
end
