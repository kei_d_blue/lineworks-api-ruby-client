
# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # Coreネームスペース
        module Core

            # レスポンス
            module Response

                # パーサー
                module Parser
                    
                    # LINEWORKS API のデータ構造 returnValue を取得する特異メソッド を生成
                    module ReturnValue
                        def return_value
                            self.body["returnValue"]
                        end
                    end
                end
            end
        end
    end
end