require 'faraday'
require 'jwt'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # Coreネームスペース
        module Core

            # HTTPリクエスト部分を制御するクラス
            # API 2.0用に新設
            # @see https://developers.worksmobile.com/jp/reference/api-request?lang=ja
            class HttpRequest

                # 認証情報を設定するアクセサー
                attr_accessor :authorization

                # 初期化
                # 引数から設定もしくは .envから取得
                def initialize(options = {})
                    @base_url = "https://www.worksapis.com"
                    @options = options
                end
                
                private
                # デフォルトの接続設定を追加
                def get_connection(url: @base_url)
                    ::Faraday.new(:url => url) do |faraday|
                        faraday.request :url_encoded # リクエストパラメータを URL エンコードする
                        faraday.response :logger 
                        faraday.response :json

                        if block_given?
                            yield(faraday)
                        end
                        faraday.adapter  Faraday.default_adapter
                    end
                end

                # 共通のヘッダーを設定する
                def set_default_headers(req)
                    p @authorization
                    req.headers['Content-Type'] = 'application/json;charset=UTF-8'
                    req.headers['Authorization'] = "Bearer #{@authorization["access_token"]}"
                end                 
            end
        end
    end
end