require 'faraday'
require 'faraday_middleware'
require 'jwt'

# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # Coreネームスペース
        module Core

            # 基本サービス
            class BasicService

                # 認証情報を設定するアクセサー
                attr_accessor :authorization

                # 初期化
                # 引数から設定もしくは .envから取得
                def initialize(options = {})
                    @base_url = "https://apis.worksmobile.com"
                    @options = options
                end
                
                private
                # デフォルトの接続設定を追加
                def get_connection(url: @base_url)
                    ::Faraday.new(:url => url) do |faraday|
                        faraday.request :url_encoded # リクエストパラメータを URL エンコードする
                        #faraday.response :logger if @options[:logger]
                        faraday.response :json

                        if block_given?
                            yield(faraday)
                        end
                        faraday.adapter  Faraday.default_adapter
                    end
                end

                # 共通のヘッダーを設定する
                def set_default_headers(req)
                    req.headers['Content-Type'] = 'application/json;charset=UTF-8'
                    req.headers['consumerKey'] = @authorization["consumer_key"]
                    req.headers['Authorization'] = "Bearer #{@authorization["access_token"]}"
                end
                
                # app_idを含んだurlを設定する
                def set_url(req, url)
                    req.url "/#{@authorization["app_id"]}#{url}"
                end
                 
            end
        end
    end
end