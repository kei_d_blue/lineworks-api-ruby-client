# Lineworksネームスペース
module Lineworks

    # Apiネームスペース
    module Apis

        # Coreネームスペース
        module Core

            # LINEWORKS API で失敗した時のエラー
            class InvalidResponseException < StandardError
                attr_accessor :body

                def initialize(message:,body:)
                    super(message)
                    self.body = body
                end
            end
        end
    end
end