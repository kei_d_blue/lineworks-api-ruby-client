lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "lineworks/apis/version"

Gem::Specification.new do |spec|
  spec.name          = "lineworks-api-ruby-client"
  spec.version       = Lineworks::Apis::VERSION
  spec.authors       = ["Keita Suzuki"]
  spec.email         = ["keita.suzuki.6997@gmail.com"]

  spec.summary       = %q{REST client for LINEWORKS API}
  spec.description   = %q{REST client for LINEWORKS API}
  spec.homepage      = "https://bitbucket.org/kei_d_blue/lineworks-api-ruby-client"

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] =  spec.homepage# "TODO: Put your gem's public repo URL here."
  spec.metadata["changelog_uri"] =  spec.homepage#"TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'faraday', '2.11.0'
  spec.add_dependency 'icalendar', '=2.5.3'
  spec.add_dependency 'activesupport', '>=5.1.2'
  spec.add_dependency 'multi_json', '~> 1.0'
  spec.add_dependency 'jwt', '2.2.2'
    
  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "dotenv"
end
